from kafka import KafkaConsumer
import configparser
import sys
import json

if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read(sys.argv[1])

    mean = 0
    n = 0

    consumer = KafkaConsumer(config['topics']['stats'],                   # Topic name
        bootstrap_servers = config['kafka']['brokers'],                        # List of brokers passed from the command line
        value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
        key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
    )

    for msg in consumer:
        mean = (mean*n + msg.value['ARE'])/(n+1)
        n += 1

        print('Received %d prediction for cascade %s (ARE= %s, T obs= %s), mean ARE is %f'
        %(n, msg.value['cid'], msg.value['ARE'], msg.value['T_obs'],mean))

