from collections import OrderedDict
import kafka
from kafka import KafkaConsumer, KafkaProducer
import json
import configparser
import sys
import argparse
import logger


class Tweet :
    # defining the tweet class
    def __init__(self, txt):
        self.source=txt.key 
        self.type=txt.value['type']
        self.msg=txt.value['msg']
        self.tid=txt.value['tweet_id']
        self.info=txt.value['info']
        self.timestamp=txt.value['t']
        self.magnitude=txt.value['m']

class Cascade :
    #Cascade is defined by the messagee of the first tweet it's message and list of tweet timestamps and magnitudes
    # param : The first tweet of the cascade
    def __init__(self,Tweet):
        self.cid = Tweet.tid # Cascade id
        self.msg = Tweet.msg # message of the first tweet
        self.tweets = [(Tweet.timestamp, Tweet.magnitude)] 

    #implement cascade with tweets    
    def addTweettoCascade(self, Tweet) :
        self.tweets.append((Tweet.timestamp, Tweet.magnitude))

    def CascadeSeries(self,ObservationWindow):
        return {'type': 'serie', 'cid': self.cid, 'msg': self.msg, 'T_obs': ObservationWindow , 'tweets': self.tweets}

    def CascadePropreties(self):
        return {'type': 'size', 'cid': self.cid, 'n_tot': len(self.tweets), 't_end': self.tweets[-1][0]}


class Processor : #pour chaque source un processeur 
    def __init__(self,ObservationWindows):        
        self.Cascades = OrderedDict() # Liste de cascades pour la meme source et our chaque cascade un cid
        self.Partial_Cascades = {} #Liste contenant pour chaque ObsWin les cascades id actifs pour cette ObsWin
        for Obs in ObservationWindows :
            self.Partial_Cascades[Obs] = []
    


    def AddCascade(self,Tweet,ObservationWindows): #initiate Cascade and add it to lists
        self.Cascades[Tweet.tid] = Cascade(Tweet) #
        for Obs in ObservationWindows:
            self.Partial_Cascades[Obs].append(Tweet.tid)


    def AddRetweetToCascade(self,Tweet): # Complete the cascade object
        self.Cascades[Tweet.tid].addTweettoCascade(Tweet)
        
    def ProcessTweet(self,Tweet,ObservationWindows):
        if Tweet.type == "tweet":
            self.AddCascade(Tweet,ObservationWindows)
            
        else :
            if Tweet.tid in self.Cascades:
                self.AddRetweetToCascade(Tweet)



    def Process(self,Tweet,ObservationWindows,TerminateTime,MinCascadeSize):
        TerminatedCascades = {}
        Partial_Cascades = []
        
        CascadeTwo=self.Cascades.copy()
        for cascade in CascadeTwo :
            if Tweet.timestamp - self.Cascades[cascade].tweets[-1][0] > TerminateTime : #Check if terminated
                if len(self.Cascades[cascade].tweets) > MinCascadeSize : # if size ok
                    TerminatedCascades[cascade] = CascadeTwo[cascade].CascadePropreties() 

                    del self.Cascades[cascade]
            else:
                self.ProcessTweet(Tweet,ObservationWindows)

        for Obs in ObservationWindows: #Checking all Obs Win
            for cascade in self.Partial_Cascades[Obs]: # Checking cascade active for a specific Window
                if cascade in self.Cascades: # Chheking if not terminated
                    if Tweet.timestamp - self.Cascades[cascade].tweets[0][0] > Obs: #Checking if Window elapsed
                        if len(self.Cascades[cascade].tweets) > MinCascadeSize: #cheking if min is qpproved
                            Partial_Cascades.append(self.Cascades[cascade].CascadeSeries(Obs)) #add Cascade series to Partial Cascadee
                        self.Partial_Cascades[Obs].remove(cascade)
                else :
                    self.Partial_Cascades[ObservationWindow].remove(cascade)
    
        return TerminatedCascades, Partial_Cascades



class Collector:
    def __init__(self,params):
        self.params = params
        self.producer = KafkaProducer(bootstrap_servers=self.params['kafka']['brokers'],value_serializer=lambda v: json.dumps(v).encode('utf-8') )
        self.consumer = KafkaConsumer(self.params['topics']['in'],bootstrap_servers=self.params['kafka']['brokers'], auto_offset_reset='earliest',value_deserializer=lambda v: json.loads(v.decode('utf-8')))
        self.listprocessors = {}
        

    def SendCascadePropreties(self,TerminatedCascades):
        for cascade in TerminatedCascades:
            ObsWins = self.params['times']['observation'].split(',')
            for Obs in ObsWins:
                
                self.producer.send(self.params['topics']['properties'], key=Obs.encode('utf-8'), value=cascade)
        self.producer.flush()


    def SendCascadeSerie(self,Partial):
        for cascade in Partial:
            self.producer.send(self.params['topics']['series'], key=None, value=cascade)
            
        self.producer.flush()
      
    def Collect(self):
        if bool(self.consumer):
            for txt in self.consumer:
                tweet=Tweet(txt)
                if tweet.source in self.listprocessors :
                    processor=self.listprocessors[tweet.source]
                    ObservationWindows = [ int(obs) for obs in self.params['times']['observation'].split(',')]
                    TerminateTime =int(self.params['times']['terminated'])
                    MinCascadeSize=int(self.params['cascade']['min_cascade_size'])
                    Terminated, Partial = processor.Process(tweet,ObservationWindows,TerminateTime,MinCascadeSize) 
                    self.SendCascadePropreties(Terminated)
                    self.SendCascadeSerie(Partial)
                    

                else:
                    ObservationWindows = [ int(obs) for obs in self.params['times']['observation'].split(',')]

                    P = Processor(ObservationWindows)
                    P.ProcessTweet(tweet,ObservationWindows)
                    self.listprocessors[tweet.source] = P
            self.consumer.close()



        



if __name__ == '__main__':
    params = configparser.ConfigParser()
    params.read(sys.argv[1])
    logger=logger.get_logger('name',  debug=True,broker_list=params['kafka']['brokers'])
    collector = Collector(params)
    logger.info("creating")
    collector.Collect()
    logger.info("create")









                    












    
    


    




    

    


        
    





    



