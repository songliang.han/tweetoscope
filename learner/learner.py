#!/usr/bin/env python
# -*- coding:utf-8 -*-
import numpy as np
import scipy.optimize as optim
import json
from kafka import KafkaProducer
from kafka import KafkaConsumer
import pandas as pd
import ast
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
import pickle
import sys

sys.path.append('../logger')
import logger


"""
The learner collects samples from predictors and builds different training 
datasets for every observation window length. Periodically it trains new models 
and send them to the corresponding predictors for them to update their model.
"""

"""
Key = 600 Value = { 'type': 'sample', 'cid': 'tw23981', 'X': [ 124.312, 0.92, 203. ], 'W' : 1.21 }
X_train -- samples {[beta,n_star,G1],....}
Y_train -- labels [W,.....]

"""

#logger = logger.get_logger('learning', broker_list='localhost:9092', debug=True)
logger = logger.get_logger('learning', broker_list='kafka-service:9092', debug=True)
# dictionnary to store the samples
dataX = {600:[],1200:[]}
dataY = {600:[],1200:[]}


def input_msg(message):
    """
    return [beta, n_star, G1, W] for train random forest model

    message   -- Key = 600 Value = {'type': 'sample', 'cid': 'tw23981', 'X': [124.312, 0.92, 203.], 'W': 1.21}
    """
    T_obs = int(message.key.decode()) # byte -> int

    dict_ = eval(message.value.decode())  # str -> dict
    X = dict_['X'] # [beta,n_star,G1]
    W = dict_['W'] # W
    return T_obs, X, W


# Find the best parameters of the model, using grid research
def searchParams(X,Y):
    """
        return best model, best parameters

        X   -- input [beta, n_star, G1]
        Y   -- label W
        """
    # set the parameters for RF model we want to optimiser
    param_grid = {'max_depth': [x for x in range(1, 8, 1)],
                  'min_samples_leaf': [x for x in range(1, 5, 1)],
                  'n_estimators': [x for x in range(50, 100, 10)]}
    forest_reg = RandomForestRegressor()
    grid_search = GridSearchCV(forest_reg, param_grid, cv=5,
                               scoring='neg_mean_squared_error')
    grid_search.fit(X, Y)
    return grid_search.best_estimator_



def RF_model(message, producer, producor_topic):
    """
    kafka producer : topic: 'models'

    message   -- Key = 600 Value = {'type': 'sample', 'cid': 'tw23981', 'X': [124.312, 0.92, 203.], 'W': 1.21}
    """
    global data, nb,clf
    batch = 32
    T_obs, x, y = input_msg(message)
    dataX[T_obs].append(x) # construct the input of model[T_obs]
    dataY[T_obs].append(y) # construct the label of model[T_obs]
    X = dataX[T_obs]
    Y = dataY[T_obs]
    
    try:
        n = nb[T_obs]
    except KeyError:
        nb[T_obs] = 1

    if nb[T_obs] % batch == 0:
        clf[T_obs] = searchParams(X,Y) # best estimator
        clf[T_obs].fit(X,Y)
        producer.send(producor_topic, key=str(T_obs), value=clf[T_obs])
        logger.info("model updated for observation window " + str(T_obs))
    nb[T_obs] += 1




if __name__ == "__main__":


    ## Create dict to save models and number of T_obs we take
    clf = dict()
    nb = dict()

    # Create Consumer
    #consumer = KafkaConsumer(bootstrap_servers = 'localhost:9092',group_id = 'learner')
    consumer = KafkaConsumer(bootstrap_servers = 'kafka-service:9092',group_id = 'learner')
    consumer.subscribe(['samples'])   # Subscribe to topic

    # Create Producer message -> pickle
    producer = KafkaProducer(bootstrap_servers = 'kafka-service:9092',
                             key_serializer = str.encode,
                             value_serializer=lambda v: pickle.dumps(v))

    # send information to kafka topic
    for msg in consumer:
        RF_model(msg, producer, 'models')

    producer.flush()  # Flush: force purging intermediate buffers before leaving
