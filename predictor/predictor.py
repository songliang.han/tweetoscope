import numpy as np
import json
from json import loads, dumps
from kafka import KafkaProducer
from kafka import KafkaConsumer
import ast
import pickle
import sys
sys.path.append('../logger')
import logger

Infinity = np.inf


# --------------------------------------------------------#
'''
inputs:

Topic = cascade_properties
    Key = 600 Value = { 'type': 'parameters', 'cid': 'tw23981','msg' : 'blah blah',
                     'n_obs': 32, 'n_supp' : 120, 'params': [ 0.0423, 124.312 ] }
    Key = 600 Value = {'type' : 'size' , 'cid' : 521 , 'n_tot' : 55 , 't_end' : 6566689}

Topic = models
    Key = 600 Value = <pickle of model>

outputs

Topic = samples
Key = 600 Value = { 'type': 'sample', 'cid': 'tw23981', 'X': [ 124.312, 0.92, 203. ], 'W' : 1.21 }

Topic = stats
Key = None Value = { 'type': 'stat', 'cid': 'tw23981', 'T_obs': 600, 'ARE' : 0.93 }
ARE Mean absolute error


Topic = alert
Key = None Value = { 'type': 'alert', 'cid': 'tw23981', 'msg' : 'blah blah', 'T_obs': 600, 'n_tot' : 158 }


Topic = alert
Key = None Value = { 'type': 'alert', 'cid': 'tw23981', 'msg' : 'blah blah', 'T_obs': 600, 'n_tot' : 158 }
 '''

#logger = logger.get_logger('predictor', broker_list='localhost:9092', debug=True)
logger = logger.get_logger('predictor', broker_list='kafka-service:9092', debug=True)

model_dict = dict() # both models
cache_dict = {600: {}, 1200: {}} # store size until param arrives. dict of dicts (one for each window)
alpha = 2.3
mu = 10

def msg_serializer(message, model=False):
    ## this function a custom to deserialize
    ## the kafka message going to the cascade topic
    ## message = key, value

    #if model=True(the message is a sklearn model), we serialize with pickle
    if model:
        messageBytes = pickle.dumps(message)
    else:
        messageJSON = dumps(message)
        messageBytes = messageJSON.encode('utf-8')
    return messageBytes
def msg_deserializer(message) :
    ## this function a custom to deserialize
    ## the kafka message comming from the cascade topic
    ## message = key, value

    if message.key :
        key = loads(message.key.decode('utf-8'))
    else : 
        key = "None"
    value = ast.literal_eval(message.value.decode("UTF-8"))
    #print(key,value)
    #value = loads(message.value.decode('utf-8').replace('\'', '\"'))
    return (key, value)

#----- the learner return the RF model---#
def RF_model(msg):
    global model_dict
    T_obs = int(msg.key.decode())
    model = pickle.loads(msg.value)
    model_dict[T_obs] = model

#-------------calculate omega ----------------#
def Omega_T_obs(T_obs,params):
    global model_dict
    X = np.array(params).reshape(1, -1)  # [beta, n_star, G1]
    try:
        omega = model_dict[T_obs].predict(X)[0] # --> [omega] -> omega
    #    print('omega', omega)
    except KeyError:
        logger.info("the model for " + " T_obs= " + str(T_obs) + " does not exist")
        omega = 1
    return omega


#-------------predictor------------------#

def treatment(message):
    global null
    null = ''
    # dict_ = eval(message.value.decode())
    # key = int(message.key.decode()) # key(byte) decode
    key= msg_deserializer(message)[0]
    dict_ = msg_deserializer(message)[1]
    #print(dict_)
    try:
        other_dict_= cache_dict[key].pop(dict_['cid'])
        if dict_['type'] == 'size':
            size_dict = dict_
            params_dict = other_dict_
        else:
            size_dict = other_dict_
            params_dict = dict_
        # now we extract parameters to build produced dictionnaries
        #print(size_dict)
        #print(params_dict)
        n_tot = size_dict['n_tot']

        msg = params_dict['msg']
        n_obs = params_dict['n_obs']
        n_supp = params_dict['n_supp']
        p, beta ,G1 = params_dict['params']
        n_star = p* mu * (alpha - 1) / (alpha - 2)
        G1 = n_supp * (1-n_star)
        params = [beta, n_star, G1]
        #print(params)
        #Omega_supp = Omega_T_obs(key, t_obs)
        Omega_supp = Omega_T_obs(key, params)
        # we want N_tot = n_obs + omega * n_supp
        n_predicted = n_obs + Omega_supp * n_supp
        Omega_true = (n_tot - n_obs ) / n_supp
        # then we build the dicts
        sample_dict = { 'type': 'sample', 'cid': params_dict['cid'], 'X': params, 'W' : Omega_true }
        ARE = np.abs(n_tot - n_predicted)/n_tot
        stats_dict = { 'type': 'stat', 'cid': dict_['cid'], 'T_obs': key, 'ARE' : ARE }

        alert_dict = { 'type': 'alert', 'cid':params_dict['cid'], 'msg':msg  ,'T_obs':key, 'n_tot':n_tot  }
        #print(stats_dict)

        alert_dict = { 'type': 'alert', 'cid':dict_['cid'], 'msg':msg  ,'T_obs':key, 'n_tot':n_tot  }

        return sample_dict, stats_dict, alert_dict
    except:
        cache_dict[key][dict_['cid']] = dict_
        return 0

if __name__ == "__main__":
    # Create Consumer
    consumer = KafkaConsumer(bootstrap_servers = ['kafka-service:9092'],
                             auto_offset_reset='earliest',
                             group_id = 'predictor')
    consumer.subscribe(['cascade_properties','models'])   # Subscribe to topic

    # Create Producer
 
    # Serialize json messages
    producer = KafkaProducer(bootstrap_servers = ['kafka-service:9092'],
                             key_serializer = str.encode,
                             value_serializer = lambda v: json.dumps(v).encode('utf-8'))

    # send information to kafka topic
    for msg in consumer:
        if msg.topic == 'models':
            RF_model(msg)
            logger.info('model updated for Tobs = ' + msg.key.decode() + "s")
        else:
            result = treatment(msg)
            if result!= 0:
                sample_value, stat_value, alert_value = result

                producer.send("alert", value=alert_value, key='None')
                logger.debug(f'[ALERT] {alert_value}')
                logger.info(f'[N_Tot] {alert_value["n_tot"]}')
                producer.send('samples', key = str(alert_value['T_obs']), value = sample_value)
                logger.debug(f'[SAMPLE] {sample_value}')
                producer.send('stats', key = 'None', value= stat_value)
                logger.debug(f'[STATS] {stat_value}')
                logger.info(f'[ARE] {stat_value["ARE"]}')


                producer.send('samples', key = str(msg.key.decode()), value = sample_value)
                producer.send('stats', key = 'None', value= stat_value)
                producer.send('alerts', key = 'None', value= alert_value)

    
    producer.flush() # Flush: force purging intermediate buffers before leaving


        


