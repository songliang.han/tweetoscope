# tweetoscope
The goal of this project is to implement a scalable and maintainable application for predicting tweets popularity in real time. 

Generator: The generator oversees injecting tweets and retweets into Kafka streams.

Collector: Collects tweets and retweets and sends partial cascades according to a set of observation windows. It also manages cascade termination to send the true length of the cascade (Tweet popularity).

Estimator: Uses Hawkes process with exponential kernels to estimate the tweet popularity based on the history of retweets.

Predictor: The predictor receives random forest models trained offline on Hawkes parameters to adjust predictions. It also receives the real length of a cascade when it considered terminated and sends statistics (ARE) of the predictions alongside with samples to train the RF models.

Learner: Retrieves samples sent by the predictor from Kafka streams, when a mini batch of samples for a specific observation window is received, it triggers the training of the models (each observation window is associated with a model). 

Monitor: retrieves the ARE metric from Kafka streams and sent it to a push gateway.

Dashboard: retrieves predictions and sends alerts to a push gateway.

# setting up
pip3 install requirements.txt
# docker compose
cd ..
sudo apt install docker-compose
cd dockercompose
docker login gitlab-student.centralesupelec.fr:4567
docker-compose -f docker-compose-middleware.yml
docker-compose -f docker-compose-services.yml up

# cpu connection
ssh cpusdi1_34@phome.metz.supelec.fr

<password>
cpu2020sdi1

ssh <assigned host>

# copy files to cluster
scp deployment/middleware_deployment.yml cpusdi1_34@phome.metz.supelec.fr:/usr/users/cpusdi1/cpusdi1_34

scp deployment/service_deployment.yml cpusdi1_34@phome.metz.supelec.fr:/usr/users/cpusdi1/cpusdi1_34

# run project
kubectl -n cpusdi1-34-ns apply -f middleware_deployment.yml

kubectl -n cpusdi1-34-NS apply -f service_deployment.yml

kubectl -n cpusdi1-34-ns get pods 

# Video link
https://clipchamp.com/watch/eznhHi0f1vJ

